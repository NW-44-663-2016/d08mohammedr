using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Mohammed.Models;

namespace D08Mohammed.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160302055816_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Mohammed.Models.Location", b =>
                {
                    b.Property<int>("LocationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Mohammed.Models.Park", b =>
                {
                    b.Property<int>("ParkID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LocationID");

                    b.Property<double>("ParkArea");

                    b.Property<string>("ParkName");

                    b.HasKey("ParkID");
                });

            modelBuilder.Entity("D08Mohammed.Models.Park", b =>
                {
                    b.HasOne("D08Mohammed.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
