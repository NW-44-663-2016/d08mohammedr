﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;


namespace D08Mohammed.Models
{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;   // DB already seeded
            }
            if (context.Parks.Any())
            {
                return;
            }
            Location l1=
         new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            Location l2=
            new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            Location l3 =
            new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            Location l4=
            new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            Location l5=
            new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            Location l6=
            new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };

            context.Locations.AddRange(l1, l2, l3, l4, l5, l6);

            context.Parks.AddRange(new Park() { ParkArea = 4000, ParkName = "Main Street",LocationID=l1.LocationID },
                new Park() { ParkArea = 5000, ParkName = "Second Street",LocationID = l2.LocationID },
                new Park() { ParkArea = 6500, ParkName = "Main City", LocationID = l3.LocationID },
                new Park() { ParkArea = 5400, ParkName = "Cityville", LocationID = l4.LocationID },
                new Park() { ParkArea=5800, ParkName="TK Hills", LocationID = l5.LocationID },
                new Park() { ParkArea = 6200, ParkName = "Starling City", LocationID = l6.LocationID }
            
            );


            
        context.SaveChanges();
        }
    }
}
