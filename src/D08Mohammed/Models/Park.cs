﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace D08Mohammed.Models
{
    public class Park
    {
        [ScaffoldColumn(false)]
        public int ParkID { get; set; }

        [Display(Name ="Name of the park")]
        public string ParkName { get; set; }

        [Display(Name ="Area of the park")]
        public double ParkArea { get; set; }
        
        public int LocationID { get; set; }
       
        public virtual Location Location { get; set; }


    }
}
