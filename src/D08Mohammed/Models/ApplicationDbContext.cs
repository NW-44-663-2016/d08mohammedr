﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace D08Mohammed.Models
{
    public class ApplicationDbContext : DbContext
    {

        public DbSet<Location> Locations { get; set; }

        public DbSet<Park> Parks { get; set; }
    }
}

