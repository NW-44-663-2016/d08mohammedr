using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Mohammed.Models;

namespace D08Mohammed.Controllers
{
    [Produces("application/json")]
    [Route("api/Parks")]
    public class ParksController : Controller
    {
        private ApplicationDbContext _context;

        public ParksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Parks
        [HttpGet]
        public IEnumerable<Park> GetParks()
        {
            return _context.Parks;
        }

        // GET: api/Parks/5
        [HttpGet("{id}", Name = "GetPark")]
        public async Task<IActionResult> GetPark([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Park park = await _context.Parks.SingleAsync(m => m.ParkID == id);

            if (park == null)
            {
                return HttpNotFound();
            }

            return Ok(park);
        }

        // PUT: api/Parks/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPark([FromRoute] int id, [FromBody] Park park)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != park.ParkID)
            {
                return HttpBadRequest();
            }

            _context.Entry(park).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParkExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Parks
        [HttpPost]
        public async Task<IActionResult> PostPark([FromBody] Park park)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Parks.Add(park);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ParkExists(park.ParkID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetPark", new { id = park.ParkID }, park);
        }

        // DELETE: api/Parks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePark([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Park park = await _context.Parks.SingleAsync(m => m.ParkID == id);
            if (park == null)
            {
                return HttpNotFound();
            }

            _context.Parks.Remove(park);
            await _context.SaveChangesAsync();

            return Ok(park);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParkExists(int id)
        {
            return _context.Parks.Count(e => e.ParkID == id) > 0;
        }
    }
}